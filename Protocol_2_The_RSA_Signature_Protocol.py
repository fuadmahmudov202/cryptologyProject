from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA

# Generate Alice's RSA key pair (2048-bit)
alice_key = RSA.generate(2048)
alice_private_key = alice_key.export_key()
alice_public_key = alice_key.publickey().export_key()

# Simulate Alice sending her public key to Bob
# Simulate Bob receiving Alice's public key
# Alice signs a message with her private key
message = "This is a signed message from Alice."
message_bytes = message.encode('utf-8')
hash_obj = SHA256.new(message_bytes)
signature = pkcs1_15.new(alice_key).sign(hash_obj)

# Alice sends the message and signature to Bob
# Bob receives the message and signature from Alice
# Bob has already received Alice's public key
# Bob verifies the signature using Alice's public key
hash_obj = SHA256.new(message_bytes)
try:
    pkcs1_15.new(RSA.import_key(alice_public_key)).verify(hash_obj, signature)
    print("Signature is valid. Message from Alice:", message)
except (ValueError, TypeError):
    print("Signature is invalid. Message may have been tampered with.")

