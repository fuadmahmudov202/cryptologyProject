import random
import AES as aes
import RSA as rsa
import os
def clean_files():
    file_list = ['bob_private_key.txt', 'bob_public_key.txt', 'decrypted.txt', 'cipher_sym_key.txt', 'ciphertext.txt',
                 'plaintext.txt']
    for name in file_list:
        if os.path.exists(name):
            try:
                os.remove(name)
                print(f"File {name} deleted successfully.")
            except Exception as e:
                print(f"Error deleting file {name}: {e}")


clean_files()
# bob's side
def send_pub_key():
    print("Sending Bob's public key...")
    pub_key, priv_key = rsa.generate_keys(2048)
    with open('bob_public_key.txt', 'w') as f:
        f.write(str(pub_key))
    with open('bob_private_key.txt', 'w') as f:  # bob stores his private key to use it later in decryption
        f.write(str(priv_key))


def decrypt_sym_key(cipher_k, priv_key):
    print("\nDecrypting the encrypted symmetric key using Bob's private key...")
    k = ""
    for c in cipher_k:
        if c != '\n':
            k += str(rsa.decrypt(int(c.strip()), priv_key))  # using RSA decryption
    return k


def decrypt_message(cipher, key):
    print("\nDecrypting the ciphertext using the symmetric key...")

    decrypted_hex = ''
    decrypted_plain = ''
    for c in cipher:
        message = aes.decrypt(int(c.strip(), 16), int(key, 16))  # using AES decryption
        decrypted_hex += hex(message)[2:]
        decrypted_plain += bytes.fromhex(hex(message)[2:]).decode('ascii')

    with open('decrypted.txt', 'a') as f:
        f.write("Decrypted value in Hex:\n\t0x" + decrypted_hex)
        f.write("\nDecrypted value in Plaintext:\n\t" + decrypted_plain)

    return decrypted_plain


# alice's side
def generate_sym_key(n):
    print("\nAlice generating symmetric key...")
    key = int("1" + "".join([str(random.choice([0, 1])) for i in range(n - 1)]), 2)
    return key


def send_encrypted_sym_key(key, pub_key):
    print("\nAlice encrypting symmetric key using Bob's public key...")
    blocks = rsa.divide_into_blocks(str(key), pub_key)
    with open('cipher_sym_key.txt', 'w') as f:
        for b in blocks:
            cipher_k = rsa.encrypt(int(str(b)), pub_key)  # using RSA encryption
            f.write(str(cipher_k) + "\n")
    print("\nSending the encrypted symmetric key to Bob...")


def send_encrypted_message(x, key):
    hex_plain_state = aes.string_to_hex(x)
    blocks = aes.divide_into_blocks(hex_plain_state)
    for b in blocks:
        b = int("0x" + str(b), 16)
        cipher_x = aes.encrypt(b, int(key, 16))  # using AES encryption
        with open('ciphertext.txt', 'a') as f:
            f.write(hex(cipher_x) + '\n')


# Writing the message in a file:
to_encrypt = """Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."""
with open("plaintext.txt", "w") as file:
    file.write(to_encrypt)

# bob sends his public key:
send_pub_key()

# alice generates a random symmetric key
key = generate_sym_key(128)

# alice uses bob's public key to encrypt the symmetric key
pub_key = open('bob_public_key.txt', 'r').read().strip()
send_encrypted_sym_key(key, eval(pub_key))

# alice encrypts and sends the message encrypted using the symmetric key
message = open('plaintext.txt', 'r').read()
send_encrypted_message(message, hex(key))

# bob receives the encrypted symmetric key and decrypts it
cipher_k = open('cipher_sym_key.txt', 'r').readlines()
bob_sym_key = decrypt_sym_key(cipher_k, eval(open('bob_private_key.txt', 'r').read()))

# bob then receives and decrypts the ciphertext received by alice using the symmetric key he just decrypted
cipher_x = open('ciphertext.txt', 'r').readlines()
decrypted_message = decrypt_message(cipher_x, hex(int(bob_sym_key)))

print('\nplaintext was:\n\t' + message)
print('\nafter decryption bob got message:\n\t' + decrypted_message)

if (message == decrypted_message):
    print('\nProtocol worked!')
else:
    print('\nAn error might have occurred in some step, recheck again!')
