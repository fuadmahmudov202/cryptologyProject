import math
import random

def extended_euclide(a, b):
    (a1, a2, a3) = 1, 0, a
    (b1, b2, b3) = 0, 1, b
    
    while b3 != 1:
        q = int(a3/b3)
        (t1, t2, t3) = (a1 - q*b1, a2 - q*b2, a3 - q*b3)
        (a1, a2, a3) = (b1, b2, b3)
        (b1, b2, b3) = (t1, t2, t3)
    while b2<0:
        b2+=a
    return b2

def generate_odd_number(bits):
    number = "1" #to make sure the number is bits long
    for _ in range(bits-2):
        number+=random.choice(["0", "1"])
     
    number+="1" #to make sure the number is odd
    return int(number, 2)

def is_prime(n, k=40):
    if n <= 1:
        return False
    if n <= 3:
        return True
    r, d = 0, n - 1
    while d % 2 == 0:
        r += 1
        d //= 2
    for _ in range(k):
        a = random.randint(2, n - 2)
        x = pow(a, d, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False

    return True

def generate_big_prime(bits):
    num = generate_odd_number(bits)

    while not is_prime(num):
        num+=2
    return num


def generate_keys(bits):
    p = generate_big_prime(int(bits/2))
    q = generate_big_prime(int(bits/2))
    n = p*q
    phi_n = (p-1)*(q-1)

    e = random.randint(3, phi_n)

    while math.gcd(e, phi_n)!=1:
        e = random.randint(3, phi_n)
    
    d = extended_euclide(phi_n, e)

    private = (d, n)
    public = (e, n)

    return (public, private)


def divide_into_blocks(message, pub_key):
    e, n = pub_key
    block_size = len(str(n)) - 1
    n_blocks = int(len(message)/block_size)+1 if len(message)%block_size!=0 else int(len(message)/block_size)
    blocks = [message[i*block_size:block_size*(i+1)] for i in range(n_blocks)]

    while len(blocks[-1])<block_size:
        blocks[-1]='0'+blocks[-1]
    return blocks

def encrypt(message, pub_key):
    e, n = pub_key
    c = pow(message, e, n)
    return c

def decrypt(cipher, priv_key):
    d, n = priv_key
    m = pow(cipher, d, n)
    return m

pb, pr = generate_keys(512)