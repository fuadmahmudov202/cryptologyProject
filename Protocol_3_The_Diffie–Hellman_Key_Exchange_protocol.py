from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from Crypto.Random import random

# Function to perform Diffie-Hellman Key Exchange
def diffie_hellman():
    # Shared prime (p) and generator (g)
    p = 23
    g = 5

    # Alice chooses a random private key
    a_private_key = random.randint(1, p-1)
    A = pow(g, a_private_key, p)  # Compute corresponding public key

    # Bob chooses a random private key
    b_private_key = random.randint(1, p-1)
    B = pow(g, b_private_key, p)  # Compute corresponding public key

    # Exchange public keys
    # Assume public keys are exchanged securely in a real scenario
    # In practice, use secure channels like TLS for key exchange

    # Compute common secrets
    common_secret_A = pow(B, a_private_key, p)
    common_secret_B = pow(A, b_private_key, p)

    # The common secrets should be the same for both Alice and Bob
    assert common_secret_A == common_secret_B
    return common_secret_A


# Function to perform AES encryption and decryption
def aes_encrypt_decrypt(key, plaintext):
    cipher = AES.new(key, AES.MODE_CBC)
    ciphertext = cipher.encrypt(pad(plaintext.encode(), AES.block_size))
    return ciphertext, cipher.iv

def aes_decrypt(key, ciphertext, iv):
    cipher = AES.new(key, AES.MODE_CBC, iv)
    decrypted_text = unpad(cipher.decrypt(ciphertext), AES.block_size)
    return decrypted_text.decode()


# Main function
def main():
    # Diffie-Hellman Key Exchange
    shared_key = diffie_hellman()

    # Use the shared key for AES encryption and decryption
    plaintext = "Hello, Bob! This is a secret message."
    ciphertext, iv = aes_encrypt_decrypt(shared_key.to_bytes(16, 'big'), plaintext)
    decrypted_text = aes_decrypt(shared_key.to_bytes(16, 'big'), ciphertext, iv)

    print("Original message:", plaintext)
    print("Encrypted message:", ciphertext)
    print("Decrypted message:", decrypted_text)


if __name__ == "__main__":
    main()
