from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

# Generate Alice's RSA key pair (2048-bit)
alice_key = RSA.generate(2048)
alice_private_key = alice_key.export_key()
alice_public_key = alice_key.publickey().export_key()
# Generate Bob's RSA key pair (2048-bit)
bob_key = RSA.generate(2048)
bob_private_key = bob_key.export_key()
bob_public_key = bob_key.publickey().export_key()

# Simulate Alice sending her public key to Bob
# Simulate Bob sending his public key to Alice

# Alice encrypts a message using AES with a random key and sends it to Bob
plaintext = "Hello, Bob! This is a secret message."
print("plaintext is ==>",plaintext)
aes_key = get_random_bytes(16)  # 128-bit AES key
aes_cipher = AES.new(aes_key, AES.MODE_EAX)
ciphertext, tag = aes_cipher.encrypt_and_digest(plaintext.encode())
print("ciphertext is ==>",ciphertext)

# Alice encrypts the AES key using Bob's public RSA key
bob_rsa_key = RSA.import_key(bob_public_key)
rsa_cipher = PKCS1_OAEP.new(bob_rsa_key)
encrypted_aes_key = rsa_cipher.encrypt(aes_key)

# Alice sends encrypted_aes_key and ciphertext to Bob (In a real scenario, you'd send them securely)

# Bob decrypts the AES key using his private RSA key
bob_private_rsa_key = RSA.import_key(bob_private_key)
rsa_cipher = PKCS1_OAEP.new(bob_private_rsa_key)
decrypted_aes_key = rsa_cipher.decrypt(encrypted_aes_key)

# Bob decrypts the ciphertext using the AES key
aes_cipher = AES.new(decrypted_aes_key, AES.MODE_EAX, nonce=aes_cipher.nonce)
decrypted_plaintext = aes_cipher.decrypt_and_verify(ciphertext, tag)

print("Decrypted Message by Bob==>", decrypted_plaintext.decode())
